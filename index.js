/**
 * Promise
 * @constructor
 * @abstract
 */
var Promise = function() {
  this.serializeSpecial="promise"
  this.resolved=undefined
  this.value=undefined
  this.local=undefined
}

/**
 * Sets callbacks called when promise ends
 * @param {resultCallback} resultCb callback called when result appears
 * @param {failureCallback} [failCb] callback called on failure
 * @param {progressCallback} [progressCb] callback called on progress
 * @returns {Promise} promise of result of resultCallback function
 */
Promise.prototype.then=function(resultCb,failCb,progressCb) {
  throw new Error('Not implemented')
}
/**
 * Result Callback
 * @callback Promise~resultCallback
 * @param {*} result
 */
/**
 * Failure Callback
 * @callback Promise~failureCallback
 * @param {*} error
 */
/**
 * Progress Callback
 * @callback Promise~resultCallback
 * @param {number} progress Progress in range 0.0 - 1.0
 */

/**
 * Sets failure callback
 * @param {failureCallback} [failCb] callback called on failure
 */
Promise.prototype.fail=function(failCb) {
  this.then(undefined,failCb)
}

/**
 * @param {failureCallback} failCb callback called on failure
 * @param {progressCallback} progressCb callback called on progress
 */
Promise.prototype.progress=function(progressCb) {
  this.then(undefined,undefined,progressCb)
}

/**
 * Calls method of promised result
 * @param {string} methodName
 * @param {...*} [param1] Parameters
 * @returns Promise of result of called method
 */
Promise.prototype.callMethod = function(methodName,param1,param2) {
  return this.applyMethod(methodName,Array.prototype.slice.call(arguments,1))
}

/**
 * Applies method of promised result
 * @param {string} methodName
 * @param {...*} [param1] Parameters
 * @returns Promise of result of called method
 */
Promise.prototype.applyMethod = function(methodName,params) {
  throw new Error('Not implemented')
}

/**
 * Removes promise - user when promise result is not longer needed
 */
Promise.prototype.remove = function() {
}

/**
 * Promise that is failed from the beginning
 * @param error
 * @constructor
 * @augments Promise
 */
var FailedPromise = function(error) {
  Promise.call(this)
  this.resolved=true
  this.error=error
  this.local=true
}
FailedPromise.prototype = Object.create(Promise.prototype)
FailedPromise.prototype.then=function(resultCb,failCb,progressCb) {
  if(failCb) failCb(this.error)
}
FailedPromise.prototype.applyMethod = function(methodName,params) {
  return this
}

/**
 * Promise that is resolved from the beginning
 * @param value
 * @constructor
 * @augments Promise
 */
var ResolvedPromise = function(value) {
  Promise.call(this)
  this.resolved=true
  this.value=value
  this.local=true
}
ResolvedPromise.prototype = Object.create(Promise.prototype)
ResolvedPromise.prototype.then=function(resultCb,failCb,progressCb) {
  if(resultCb) return promised(resultCb(this.value))
}
ResolvedPromise.prototype.applyMethod = function(methodName,params) {
  try {
    return promised(this.value[methodName].apply(this.value,params))
  } catch (e) {
    console.error(e.stack || e)
    return new FailedPromise(e.stack || e)
  }
}

/**
 * makes promise from promise or value
 * @param {*} anything
 * @returns Promise
 */
var promised = function(anything) {
  if(anything instanceof Promise) {
    return anything
  }
  return new ResolvedPromise(anything)
}

/**
 * Promise that will be resolved in the future
 * @param error
 * @constructor
 * @augments Promise
 */
var FuturePromise = function() {
  Promise.call(this)
  this.resolved=false
  this.error=null
  this.value=undefined
  this.local=true

  this.progressListeners=[]
  this.valueListeners=[]
  this.errorListeners=[]
}
FuturePromise.prototype = Object.create(Promise.prototype)
FuturePromise.prototype.then=function(resultCb,failCb,progressCb) {
  if(this.resolved) {
    if(this.error && failCb) return failCb(this.error)
    if(resultCb) return promised(resultCb(this.value))
    if(progressCb) return progressCb(1)
  }
  var futu=new FuturePromise()
  this.progressListeners.push(function(progress) {
    if(progressCb) return futu.notify(progressCb(progress))
    futu.notify(progress)
  })
  this.valueListeners.push(function(value) {
    futu.resolve(resultCb(value))
  })
  this.errorListeners.push(function(error) {
    if(failCb) failCb(error)
    futu.resolveError(error)
  })
  return futu
}
FuturePromise.prototype.applyMethod = function(methodName,params) {
  if(this.resolved) {
    if(this.error) throw new Error(this.error)
    return promised(this.value[methodName].apply(this.value,params))
  }
  var futu=new FuturePromise()
  this.progressListeners.push(function(progress) {
    futu.notify(progress)
  })
  this.valueListeners.push(function(value) {
    futu.resolve(value[methodName].apply(value,params))
  })
  this.errorListeners.push(function(error) {
    futu.resolveError(error)
  })
  return futu
}
FuturePromise.prototype.resolveError = function(error) {
  this.resolved=true
  this.error=error
  this.errorListeners.forEach(function(cb) {
    cb(error)
  })
  this.progressListeners.forEach(function(cb) {
    cb(0)
  })
}
FuturePromise.prototype.resolve = function(value) {
  this.resolved=true
  this.value=value
  this.progressListeners.forEach(function(cb) {
    cb(1)
  })
  this.valueListeners.forEach(function(cb) {
    cb(value)
  })
}
FuturePromise.prototype.notify = function(progress) {
  this.progressListeners.forEach(function(cb) {
    cb(progress)
  })
}


/**
 * Tries to resolve all promises, and returns promise of array with results
 * @param {Promise[]} promises
 * @returns {Promise} promise of array with results
 */
var doAll = function(promises) {
  if(promises.length==0) return new ResolvedPromise([])
  var futu=new FuturePromise()
  var results=[]
  var progresses=[]
  var more=promises.length
  var resolved=false
  for(var i=0; i<promises.length; i++) {
    (function(i) {
      progresses[i]=0
      var promise=promises[i]
      promise.then(function(result) {
        if(resolved) return;
        results[i]=result
        more--
        if(more==0) {
          resolved=true
          futu.resolve(results)
        }
      },function(error) {
        if(resolved) return;
        resolved=true
        futu.resolveError(error)
      },function(progress){
        if(resolved) return;
        progresses[i]=progress
        futu.notify(progresses.reduce(function(a,b) {
          return a+b
        })/promises.length)
      })
    })(i)
  }
  return futu
}

/**
 * Gets Promise of Promise or Promise of Promise of Promise... Makes single Promise of value of resolved chain.
 * @param {Promise} promise
 * @returns {Promise}
 */
var flatten = function(promise) {
  if(!promise.local) return promise
  var futu=new FuturePromise()
  var onResult=function(x) {
    if(x instanceof Promise) {
      if(!promise.local) return futu.resolve(x)
      x.then(onResult,onError)
    } else futu.resolve(x)
  }
  var onError = function(err) {
    futu.resolveError(err)
  }
  promise.then(onResult,onError)
  return futu
}

var promisedResult = function(func) {
  try {
    var r=func()
  } catch(e) {
    return new FailedPromise(e)
  }
  return promised(r)
}

exports.Promise=Promise
exports.FailedPromise=FailedPromise
exports.ResolvedPromise=ResolvedPromise
exports.FuturePromise=FuturePromise
exports.promised=promised
exports.promisedResult=promisedResult
exports.doAll=doAll
exports.flatten=flatten
